
-- SUMMARY --

This module restrict user to delete node in publish state, it shows a message
on confirm form (which is configurable from setting form) Published node 
cannot be deleted. Please unpublished the node first and try again.
once user save node in draft state then user is able to delete content. 

-- REQUIREMENTS --
  No

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.


-- CONFIGURATION --

* Configure user permissions in Administration » People » Permissions:

-- CUSTOMIZATION --

* Access menu from here : admin/config/two_step_delete/settings

-- CONTACT --

Current maintainers:
* Ankush Gautam https://www.drupal.org/u/agautam
  email : ankushgautam76@gmail.com
